(function($) {
  $(document).ready(function () {
    // Scroll Down to Our Mission Block
    $('.about').on('click', function(e) {
        $('body,html').animate({ scrollTop: $('#block-block-8').offset().top - 50 }, 700);
    });

    // Active/Desactive Mobile Menu
    $('#active_mobile').on('click', function(e) {
        e.preventDefault();
        var menu = $('#navbar_mobile');
        if(menu.hasClass('active')) {
            $('#navbar_mobile').removeClass('active');
        } else {
            $('#navbar_mobile').addClass('active');
        }
    });

    // Light Slider
    // Our Members
    $('#block-block-9 .content ul').lightSlider({
        item:5,
        loop:false,
        slideMove:1,
        speed:600,
        pager: true,
        controls: false,
        responsive : [
            {
                breakpoint:800,
                settings: {
                    item:3,
                    slideMove:1,
                    slideMargin:6,
                  }
            },
            {
                breakpoint:480,
                settings: {
                    item:2,
                    slideMove:1
                  }
            }
        ]
    });
    // Posts
    var slider = $('.posts').lightSlider({
        item:4,
        loop:false,
        slideMove:1,
        speed:600,
        pager: false,
        responsive : [
            {
                breakpoint:800,
                settings: {
                    item:3,
                    slideMove:1,
                    slideMargin:6,
                  }
            },
            {
                breakpoint:480,
                settings: {
                    item:1,
                    slideMove:1
                  }
            }
        ]
    });
    $('.lSAction .lSPrev').click(function() { slider.goToPrevSlide(); }); 
    $('.lSAction .lSNext').click(function() { slider.goToNextSlide(); });
  });
})(jQuery);
