<?php
// Template for Get Involved
?>

<div id="<?php print $block_html_id; ?>" class="<?php print $classes; ?>"<?php print $attributes; ?>>
  <div class="container">
    <?php print render($title_prefix); ?>
    <?php if ($block->subject): ?>
      <h2 class="text-center"><?php print $block->subject ?></h2>
    <?php endif;?>
    <?php print render($title_suffix); ?>

    <div class="content"<?php print $content_attributes; ?>>
      <?php print $content ?>
    </div>
  </div>
</div>
