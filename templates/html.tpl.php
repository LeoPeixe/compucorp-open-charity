<!DOCTYPE html >
<html>
<head>
  <meta name="viewport" content="width=device-width, user-scalable=no">
  <?php print $head; ?>
  <title><?php print $head_title; ?></title>
  <?php print $styles ?>
  <?php print $scripts ?>
</head>
<body class="<?php print $classes; ?>" <?php print $attributes;?>>
  <?php print $page_top ?>
  <?php print $page ?>
  <?php print $page_bottom; ?>
</body>
</html>