<?php
// Template for Footer Social Networks Block
?>

<div id="<?php print $block_html_id; ?>" class="<?php print $classes; ?>"<?php print $attributes; ?>>
  <div class="row">
    <div class="col s12 m12 l12 text-center">
      <a href="https://www.facebook.com/opencharityorg" target="_blank">
        <i class="fab fa-facebook-f"></i>
      </a>
      <a href="https://twitter.com/OpenCharityOrg" target="_blank">
        <i class="fab fa-twitter"></i>
      </a>
      <a href="https://www.plus.google.com">
        <i class="fab fa-google-plus-g"></i>
      </a>
    </div>
  </div>
</div>
