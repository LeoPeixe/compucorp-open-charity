<!-- Specific Style -->
<style type="text/css">
  body {
    padding-top: 80px;
  }
  #header {
    background-color: #f2f2f2 !important;
  }
</style>
<div id="page-wrapper"><div id="page">

  <div id="header"><div class="section clearfix">

    <div class="container">
      <?php if ($logo): ?>
        <a href="<?php print $front_page; ?>" title="<?php print t('OpenCharity'); ?>" rel="home" id="logo">
          <img src="<?php print $logo; ?>" alt="<?php print t('OpenCharity'); ?>" />
        </a>
      <?php endif; ?>

      <?php print render($page['header']); ?>
    </div>

  </div></div> <!-- /.section, /#header -->
  <div class="container">
    <div class="row">
      <div class="col s12 m9 l9" id="content"><div class="section">
        <h2>The Blog</h2>
        <?php print render($page['content']); ?>
      </div></div> <!-- /.section, /#content -->
      <div class="col s12 m3 l3" id="right_bar"><div class="section">
        <?php print render($page['right_bar']); ?>
      </div></div> <!-- /.section, /#right_bar -->
    </div>
  </div>
  <div id="footer"><div class="section container">
    <?php print render($page['footer']); ?>
  </div></div> <!-- /.section, /#footer -->

</div></div> <!-- /#page, /#page-wrapper -->
