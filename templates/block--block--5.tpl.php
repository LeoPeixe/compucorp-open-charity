<?php
// Template for Header Menu
?>
<ul id="navbar">
  <li>
    <a class="btn-opac" href="https://www.meetup.com/pt-BR/Open-Charity/">
      Join Us
    </a>
  </li>
  <li>
    <a href="?q=blog">
      The Blog
    </a>
  </li>
  <li>
    <a href="/open-charity/#block-block-8" class="about">
      About Open Charity
    </a>
  </li>
</ul>
<a href="" id="active_mobile">
  <i class="fas fa-bars"></i>
</a>
<ul id="navbar_mobile">
  <li>
    <a class="btn-opac" href="https://www.meetup.com/pt-BR/Open-Charity/">
      Join Us
    </a>
  </li>
  <li>
    <a href="?q=blog">
      The Blog
    </a>
  </li>
  <li>
    <a href="/open-charity/#block-block-8" class="about">
      About Open Charity
    </a>
  </li>
</ul>