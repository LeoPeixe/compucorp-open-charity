<?php
// Template for Events Block
?>
<?php
$result = db_query("SELECT nid, title, created FROM {node} WHERE type = 'events' ORDER BY created DESC LIMIT 1");

if ($result) {
?>
<div id="<?php print $block_html_id; ?>" class="<?php print $classes; ?>"<?php print $attributes; ?>>
  <div class="container">
    <?php
    foreach ($result as $value) {
      $node = node_load($value->nid);
      $field_date_1 = strtotime($node->field_date['und'][0]['value']);
      $field_date_2 = strtotime($node->field_date['und'][0]['value2']);
      $day = date('d', $field_date_1);
      $day_suffix = 'th';
      if ($day < 10 | $day > 20)
      {
        switch ($day%10)
        {
          case 1:
            $day_suffix = 'st';
            break;
          case 2:
            $day_suffix = 'nd';
            break;
          case 3:
            $day_suffix = 'rd';
            break;
          default:
            break;
        }
      }
      $month = date('M', $field_date_1);
      $year = date('Y', $field_date_1);
      $date = $month . ' ' . $day . '<span class="sufix">' . $day_suffix . '</span> ' . $year;
      $time_init = date('H:i', $field_date_1);
      $time_end = date('H:i', $field_date_2);
    ?>
      <div class="row">
        <div class="col s12 m8 l10">
          <span class="title"><?php print $block->subject ?></span>
          <span class="text">
            <?php print $date ?> <span><?php print $time_init . ' - ' . $time_end ?></span>
          </span>
          <p class="opensans">
            <?php print $node->field_where['und'][0]['value'] ?>
          </p>
        </div>
        <div class="col s12 m4 l2">
          <a href="https://www.meetup.com/pt-BR/Open-Charity/" target="_blank">
            Register
          </a>
        </div>
      </div>
    <?php
    }
    ?>
  </div>
</div>
<?php
}
?>