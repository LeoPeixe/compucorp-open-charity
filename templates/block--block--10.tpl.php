<?php
// Template for Blog Block
?>

<div id="<?php print $block_html_id; ?>" class="<?php print $classes; ?>"<?php print $attributes; ?>>
    <?php print render($title_prefix); ?>
    <?php if ($block->subject): ?>
      <h2 class="text-center"><?php print $block->subject ?></h2>
    <?php endif;?>
    <?php print render($title_suffix); ?>

    <?php
    $result = db_query("SELECT nid, title, created FROM {node} WHERE type = 'article' ORDER BY created DESC LIMIT 0, 12");

    if ($result) {
    ?>
  <div class="container">
    <ul class="row posts">
      <?php
      foreach ($result as $value) {
        $node = node_load($value->nid);
      ?>
        <li class="col s12 m6 l3">
          <p class="title">
            <b>
              <?php print $value->title; ?>
            </b>
          </p>
          <div class="text">
            <?php print $node->body['und'][0]['value'] ?>
          </div>
          <p class="date">
            <?php print date('d M Y', $value->created); ?>
          </p>
        </li>
      <?php
      }
    ?>
    </ul>
    <div class="lSAction"><a class="lSPrev"><i class="fa fa-chevron-left"></i></a><a class="lSNext"><i class="fa fa-chevron-right"></i></a></div>
  </div>
    <?php
    } else {
    ?>
    <div class="text-center">
      No posts found
    </div>
    <?php
    }
    ?>
</div>