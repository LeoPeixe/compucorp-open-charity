<div id="page-wrapper"><div id="page">

  <div id="header"><div class="section clearfix">

    <div class="container">
      <?php if ($logo): ?>
        <a href="<?php print $front_page; ?>" title="<?php print t('OpenCharity'); ?>" rel="home" id="logo">
          <img src="<?php print $logo; ?>" alt="<?php print t('OpenCharity'); ?>" />
        </a>
      <?php endif; ?>

      <?php print render($page['header']); ?>
    </div>

  </div></div> <!-- /.section, /#header -->


  <div id="content"><div class="section container">
    <?php if ($title): ?><h1 class="title" id="page-title"><?php print $title; ?></h1><?php endif; ?>
    <?php print render($page['content']); ?>
  </div></div> <!-- /.section, /#content -->
  <div id="footer"><div class="section container">
    <?php print render($page['footer']); ?>
  </div></div> <!-- /.section, /#footer -->

</div></div> <!-- /#page, /#page-wrapper -->
